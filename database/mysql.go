package database

import (
	"fmt"

	"gitlab.com/sukrawidhyawan/jade-withdrawal/model"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

type (
	MysqlDB struct {
		connection *gorm.DB
	}
)

// NewMysqlDB to create instance of mysql function
func NewMysqlDB() *MysqlDB {
	return &MysqlDB{}
}

func (md *MysqlDB) OpenDB(username, password, host, dbName string, port int) (*gorm.DB, error) {
	// refer https://github.com/go-sql-driver/mysql#dsn-data-source-name for details
	var (
		err error
		dsn = fmt.Sprintf(
			"%s:%s@tcp(%s:%d)/%s?charset=utf8mb4&parseTime=True&loc=Local",
			username, password, host, port, dbName)
	)

	md.connection, err = gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		return nil, err
	}
	return md.connection, nil
}

func (md *MysqlDB)Migration() {
	md.connection.AutoMigrate(&model.Withdrawal{})
	md.connection.AutoMigrate(&model.Account{})
}
