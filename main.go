package main

import (
	"flag"
	"fmt"
	"log"
	"time"

	"github.com/spf13/viper"
	"gitlab.com/sukrawidhyawan/jade-withdrawal/database"
	"gitlab.com/sukrawidhyawan/jade-withdrawal/util"
	"gitlab.com/sukrawidhyawan/jade-withdrawal/withdrawalreq"
	"gitlab.com/sukrawidhyawan/jade-withdrawal/withdrawalreq/ethereum"
	"gorm.io/gorm"
)

var (
	dbUserName, dbPassword, dbName, dbIPAddress string
	dbPort, dailyLimit, weeklyLimit             int
	sqlInstance                                 *gorm.DB
	privateKeyAgent                             string
)

func initDatabase() {
	var (
		err     error
		mysqlDB = database.NewMysqlDB()
	)
	sqlInstance, err = mysqlDB.OpenDB(dbUserName, dbPassword, dbIPAddress, dbName, dbPort)
	if err != nil {
		log.Fatal(err.Error())
	}
	mysqlDB.Migration()

}

func init() {
	// Read from config file
	if err := util.ReadConfig("config", "toml"); err != nil {
		log.Fatal(err)
	}
	// Get Database Intance
	dbUserName = viper.GetString("mysqlUsername")
	dbPassword = viper.GetString("mysqlPassword")
	dbName = viper.GetString("mysqlDBName")
	dbIPAddress = viper.GetString("mysqlDBIP")
	dbPort = viper.GetInt("mysqlDBPort")

	dailyLimit = viper.GetInt("dailyLimit")
	weeklyLimit = viper.GetInt("weeklyLimit")
	privateKeyAgent = viper.GetString("privateKeyAgent")

	initDatabase()
}

func main() {
	var (
		err        error
		ethHandler = ethereum.NewETHClient(
			ethereum.EthSepoliaQuickNode,
			ethereum.EthSepoliaEtherScan,
			ethereum.EtherScanAPIKEY,
		)
		reqHandler = withdrawalreq.NewWithdrawalRequestHandler(
			sqlInstance,
			privateKeyAgent,
			uint64(dailyLimit),
			uint64(weeklyLimit),
			ethHandler,
		)
	)

	// insert example process
	insertExampleData := flag.Bool("insert-example", false, "Insert Example Data For Request Withdrawal")
	flag.Parse()
	if *insertExampleData {
		err = reqHandler.InsertExampleData()
		if err != nil {
			log.Fatal(err.Error())
		}
	}

	ethHandler.Dial()
	reqHandler.GetNewRequest()
	err = reqHandler.ProcessRequest()
	if err != nil {
		log.Fatal(err.Error())
	}

	// Checking Transaction Status
	ticker := time.NewTicker(1 * time.Second)
    done := make(chan bool)
	 go func() {
        for {
            select {
            case <-done:
				ticker.Stop()
                return
            case <-ticker.C:
				reqHandler.CheckTransactionStatus()
                fmt.Println("Tick at",)
            }
        }
    }()
	time.Sleep(1 * time.Hour)
   
    // done <- true

}
