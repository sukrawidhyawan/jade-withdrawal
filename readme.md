<h1 align="center">
    JADE WITHDRAWAL
</h1>

### Development
- Clone the repository
- install the dependencies by run `go mod vendor`
- Rename `configExample.toml` to `config.toml`
- Setup the configuration on `config.toml`
- Setup the data account on file `example-Account.json` based on wallet account
- Setup the withdrawal request on file `example.json` (note: BankAccount should be same with BankAccount on `example-Account.json`)

###  Run
- Run by Example `go run main.go --insert-example`


