package withdrawalreq

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"os"
	"sync"
	"time"

	"gitlab.com/sukrawidhyawan/jade-withdrawal/model"
	"gitlab.com/sukrawidhyawan/jade-withdrawal/withdrawalreq/ethereum"
	"gorm.io/gorm"
)

const (
	ETHCoin = "ETH"
)

type WithdrawalRequestHandler struct {
	sync.RWMutex
	requestList    []model.Withdrawal
	Accouts        map[uint]model.Account
	TrxHash []string
	DB             *gorm.DB
	DailyLimit     uint64
	WeeklyLimit    uint64
	EthPrivatyeKey string
	EthHandler     *ethereum.EthHandler
}

func NewWithdrawalRequestHandler(db *gorm.DB, privateKey string, dailyLimit, weeklyLimit uint64, ethHandler *ethereum.EthHandler) *WithdrawalRequestHandler {
	return &WithdrawalRequestHandler{
		DB:             db,
		EthPrivatyeKey: privateKey,
		DailyLimit:     dailyLimit,
		WeeklyLimit:    weeklyLimit,
		EthHandler:     ethHandler,
	}
}

func (wrh *WithdrawalRequestHandler) ProcessRequest() error {
	var (
		err       error
		account   model.Account
		hexSignTx string
		listHexSignTx []string
	)
	
	for _, req := range wrh.requestList {
		hexSignTx = ""
		account = wrh.Accouts[req.AccountID]
		if uint64(account.DailyAmount)+req.Amount > wrh.DailyLimit {
			req.Status = model.StatusFAILED
			wrh.DB.Save(&req)
			continue
		}

		if uint64(account.WeeklyAmount)+req.Amount > uint64(wrh.WeeklyLimit) {
			req.Status = model.StatusPENDING
			wrh.DB.Save(&req)
			continue
		}

		switch req.BankCode {
		case ETHCoin:
			hexSignTx, err = wrh.EthHandler.SendTransaction(req, wrh.EthPrivatyeKey)
			if err != nil {
				return err
			}
		default:
			hexSignTx, err = wrh.EthHandler.SendTransaction(req, wrh.EthPrivatyeKey)
			if err != nil {
				return err
			}
		}
		// update account amount
		account.DailyAmount = account.DailyAmount + req.Amount
		account.WeeklyAmount = account.WeeklyAmount + req.Amount
		account.LastTimeWithdrawal = time.Now()
		wrh.Accouts[req.AccountID] = account
		wrh.DB.Save(&account)

		// Update withdrawal status
		listHexSignTx = append(listHexSignTx, hexSignTx)
		req.Status = model.StatusON_PROCESS
		req.HexSignTransaction = hexSignTx
		wrh.DB.Save(&req)
	}
	wrh.Lock()
	defer wrh.Unlock()
	wrh.TrxHash = listHexSignTx
	
	return nil
}

func (wrh *WithdrawalRequestHandler)CheckTransactionStatus() error {
	wrh.Lock()
	defer wrh.Unlock()

	for key, txHex := range wrh.TrxHash{
		fmt.Println("Transaction ke ----> : ", key)
		fmt.Println("Transaction HEX : ", txHex)
		isSuccess,  err := wrh.EthHandler.CheckTransctionStatus(txHex); 
		if err != nil {
			return err
		}
		req := wrh.requestList[key]
		if isSuccess {
			req.Status = model.StatusSUCCEED
		} else {
			req.Status = model.StatusFAILED
		}
		wrh.DB.Save(&req)
	}
	wrh.TrxHash = make([]string, 0)
	return nil 
}
// GetNewRequest get a list fo request withdrawal
func (wrh *WithdrawalRequestHandler) GetNewRequest() {
	var newWithdrawalReq []model.Withdrawal
	// get the withdrawal req
	wrh.DB.Where("status = ?", model.StatusNEW).Find(&newWithdrawalReq)
	wrh.requestList = newWithdrawalReq

	// get account req
	var (
		tmpIDs     = make(map[uint]uint)
		AccountIDs []uint
		accounts   []model.Account
	)
	for _, req := range wrh.requestList {
		tmpIDs[req.AccountID] = req.AccountID
	}
	for _, id := range tmpIDs {
		AccountIDs = append(AccountIDs, id)
	}
	wrh.DB.Where(AccountIDs).Find(&accounts)

	wrh.Accouts = make(map[uint]model.Account)
	for _, acc := range accounts {
		wrh.Accouts[acc.ID] = acc
	}

	fmt.Println(newWithdrawalReq)
	fmt.Println(accounts)
}

func (wrh *WithdrawalRequestHandler) InsertExampleData() error {
	var (
		requestList []model.Withdrawal
		accountExample []model.Account
	)

	// Insert Example Account
	jsonFileAcc, err := os.Open("example-Account.json")
	if err != nil {
		return err
	}
	defer jsonFileAcc.Close()
	buytesFileAccount, err := io.ReadAll(jsonFileAcc)
	if err != nil {
		return err
	}
	json.Unmarshal(buytesFileAccount, &accountExample)
	result := wrh.DB.Create(&accountExample)
	if result.Error != nil {
		log.Fatal(err.Error())
	}

	// Insert Example Request Witdrawal
	jsonFile, err := os.Open("example.json")
	if err != nil {
		return err
	}
	defer jsonFile.Close()

	buytesFile, err := io.ReadAll(jsonFile)
	if err != nil {
		return err
	}
	json.Unmarshal(buytesFile, &requestList)
	wrh.requestList = requestList
	result = wrh.DB.Create(&requestList)
	if result.Error != nil {
		log.Fatal(err.Error())
	}
	return nil
}
