package ethereum

const (
	EthSepoliaDRPC          = "https://sepolia.drpc.org"
	EthSepoliaAllThatNode   = "https://ethereum-sepolia-rpc.allthatnode.com"
	EthSepoliaGetBlock      = "https://go.getblock.io/d55e5054105e4540afd9d4ddb5be7d71"
	EthSepoliaBlast         = "https://eth-sepolia.blastapi.io/c88d6758-0b43-4b7a-9dde-612b14327fbc"
	EthSepoliaChainstack    = "https://nd-690-281-137.p2pify.com/b39846e97b923a1a291b0ea5d76ae56f"
	EthSepoliaBlockPi       = "https://ethereum-sepolia.blockpi.network/v1/rpc/public"
	EthSepoliaQuickNode     = "https://late-purple-hill.ethereum-sepolia.quiknode.pro/9733d0f3faeb92431deeafa6ca9fcc4685368625"
	EthSepoliaPocketNetwork = "https://sepolia.rpc.grove.city/v1/821445a7"
	EthSepoliaRivet         = "https://sepolia.rpc.rivet.cloud/5f6f35bf13214c49bb9826173ce2e07d" // NOT Working
	EthSepoliaTenderly      = "https://sepolia.gateway.tenderly.co/6MO80AUwhJ4MUrrfUGdyxG"       // NOT Working - only block height

	// Checking Transaction 
	EthSepoliaEtherScan =  "https://api-sepolia.etherscan.io/api"
	EtherScanAPIKEY = "UY1WIXXN635G6Y6ADQ1NXUVM4VHNVNGFTV"

)