package ethereum

import (
	"context"
	"crypto/ecdsa"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"math/big"
	"net/http"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/ethereum/go-ethereum/params"
	"gitlab.com/sukrawidhyawan/jade-withdrawal/model"
)

type (
	EthHandler struct {
		RawURL          string
		EtherScanURL    string
		EtherScanAPIKey string
		Client          *ethclient.Client
		privateKey      *ecdsa.PrivateKey
		FromAddress     common.Address
		Nonce           uint64
	}

	EtherScanTransactionStatusReponse struct {
		Status  string
		Message string
		Result  EtherScanTransactionStatusResultReponse
	}
	EtherScanTransactionStatusResultReponse struct {
		IsError        string
		ErrDescription string
	}
)

func NewETHClient(url, etherScanURL, etherScanAPIKey string) *EthHandler {
	return &EthHandler{
		RawURL:          url,
		EtherScanURL:    etherScanURL,
		EtherScanAPIKey: etherScanAPIKey,
	}
}

func (eh *EthHandler) Dial() error {
	rpcClient, err := ethclient.Dial(eh.RawURL)
	if err != nil {
		return err
	}
	eh.Client = rpcClient
	return nil
}

func (eh *EthHandler) EthBlockNumber() (uint64, error) {
	var (
		err         error
		blockNumber uint64
	)
	if blockNumber, err = eh.Client.BlockNumber(context.Background()); err != nil {
		return blockNumber, err
	}
	return blockNumber, nil
}

func (eh *EthHandler) SetPrivateKeyAndFromAddress(str string) error {
	privateKey, err := crypto.HexToECDSA(str)
	if err != nil {
		return err
	}
	eh.privateKey = privateKey
	publicKeyECDSA, ok := eh.privateKey.Public().(*ecdsa.PublicKey)
	if !ok {
		return errors.New("error casting public key to ECDSA")
	}
	eh.FromAddress = crypto.PubkeyToAddress(*publicKeyECDSA)
	fmt.Println("eh.FromAddress :", eh.FromAddress)
	return nil
}

func (eh *EthHandler) GetSenderAddress(privateKey string) (common.Address, error) {
	if eh.privateKey == nil {
		return eh.FromAddress, errors.New("privatekey not set")
	}
	return eh.FromAddress, nil
}

func (eh *EthHandler) GetNonce() (uint64, error) {
	fmt.Println("GetNonce-->")
	var (
		err error
	)
	eh.Nonce, err = eh.Client.PendingNonceAt(context.Background(), eh.FromAddress)
	fmt.Println("err GetNonce", err)
	if err != nil {
		return 0, err
	}
	fmt.Println("eh.Nonce :", eh.Nonce)
	return eh.Nonce, err
}

// Build Transaction
func (ec *EthHandler) PrivateKey(str string) {
	privateKey, err := crypto.HexToECDSA(str)
	if err != nil {
		log.Fatal(err)
	}
	ec.privateKey = privateKey
}

func (ec *EthHandler) GetFromAddress(privateKey string) {
	var err error
	ec.privateKey, err = crypto.HexToECDSA(privateKey)
	if err != nil {
		log.Fatal(err)
	}
	publicKeyECDSA, ok := ec.privateKey.Public().(*ecdsa.PublicKey)
	if !ok {
		log.Fatal("error casting public key to ECDSA")
	}
	ec.FromAddress = crypto.PubkeyToAddress(*publicKeyECDSA)
	fmt.Println("FromAddress : ", ec.FromAddress)
}

func (ec *EthHandler) SendTransaction(withdrawalreq model.Withdrawal, privateKey string) (string, error) {
	var (
		hexSignedTx string
		value       = big.NewInt(int64(withdrawalreq.Amount))
		toAddress   = common.HexToAddress(withdrawalreq.BankAccount)
		gasLimit    = uint64(21000)
		gasPrice    = big.NewInt(30000000000)
	)
	ec.SetPrivateKeyAndFromAddress(privateKey)
	ec.GetNonce()
	tx := types.NewTransaction(ec.Nonce, toAddress, value, gasLimit, gasPrice, nil)

	chainID, err := ec.Client.NetworkID(context.Background())
	if err != nil {
		return hexSignedTx, err
	}
	fmt.Println("chainID -->", chainID)
	signedTx, err := types.SignTx(tx, types.NewEIP155Signer(chainID), ec.privateKey)
	if err != nil {
		return hexSignedTx, errors.New(fmt.Sprintln("err sign transaction ", err.Error()))
	}
	fmt.Println("signedTx ->>", signedTx.Hash().Hex())

	err = ec.Client.SendTransaction(context.Background(), signedTx)
	if err != nil {
		return hexSignedTx, errors.New(fmt.Sprintln("err send transaction ", err.Error()))
	}
	hexSignedTx = signedTx.Hash().Hex()
	fmt.Printf("tx sent: %s", signedTx.Hash().Hex())
	return hexSignedTx, nil
}

func (eh *EthHandler) CheckTransctionStatus(hexTx string) (bool,  error) {
	var (
		isSuccess bool 
		buildEndPoint = fmt.Sprintf("%s?module=transaction&action=getstatus&txhash=%s&apikey=%s", eh.EtherScanURL, hexTx, eh.EtherScanAPIKey)
		res, err      = http.Get(buildEndPoint)
	)
	if err != nil {
		return isSuccess, err
	}

	responseData, err := io.ReadAll(res.Body)
	if err != nil {
		return isSuccess, err
	}
	var response EtherScanTransactionStatusReponse
	json.Unmarshal(responseData, &response)
	fmt.Println("response.Result.IsError :", response.Result.IsError)
	if response.Result.IsError == "0"{
		isSuccess = true
	}
	fmt.Println("isSuccess :", isSuccess)
	return isSuccess, nil
}
func (eh *EthHandler) weiToEther(wei *big.Int) *big.Float {
	return new(big.Float).Quo(new(big.Float).SetInt(wei), big.NewFloat(params.Ether))
}

func (eh *EthHandler) EthToWei(ethValue *big.Int) *big.Int {
	return new(big.Int).Mul(ethValue, big.NewInt(params.Ether))
}
