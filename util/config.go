package util

import "github.com/spf13/viper"

// ReadConfig to read config from file
func ReadConfig(fileName, fileType string) error {
	viper.SetConfigName(fileName)
	viper.SetConfigType(fileType)
	viper.AddConfigPath("./")

	if err := viper.ReadInConfig(); err != nil {
		return err
	}
	return nil
}