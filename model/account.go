package model

import (
	"time"

	"gorm.io/gorm"
)

type Account struct {
	gorm.Model
	ID                 uint `gorm:"primaryKey"`
	BankCode           string
	BankAccount        string
	WeeklyAmount       uint64
	DailyAmount        uint64
	LastTimeWithdrawal time.Time
}
