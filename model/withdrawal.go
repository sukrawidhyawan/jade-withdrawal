package model

import (
	"gorm.io/gorm"
)

type TypeWithdrawal string
type Status string

const (
	DEPOSIT          TypeWithdrawal = "DEPOSIT"
	WITHDRAW         TypeWithdrawal = "WITHDRAW"
	StatusSUCCEED    Status         = "SUCCEED"
	StatusON_PROCESS Status         = "ON_PROCESS"
	StatusFAILED     Status         = "FAILED"
	StatusERROR      Status         = "ERROR"
	StatusTO_PROCESS Status         = "TO_PROCESS"
	StatusNEW        Status         = "NEW"
	StatusPENDING    Status         = "PENDING"
	StatusPAID       Status         = "PAID"
	StatusSETTLED    Status         = "SETLLED"
	StatusEXPIRED    Status         = "EXPIRED"
)

type Withdrawal struct {
	gorm.Model
	ID                 uint           `gorm:"primaryKey"`
	ReferenceID        string         `gorm:"type:varchar(64)"`
	CustomerName       string         `gorm:"type:varchar(64)"`
	Type               TypeWithdrawal `gorm:"type:enum('DEPOSIT', 'WITHDRAW')"`
	Amount             uint64
	Status             Status `gorm:"type:enum('SUCCEED', 'ON_PROCESS', 'FAILED', 'ERROR', 'TO_PROCESS', 'NEW', 'PENDING', 'PAID', 'SETLLED', 'EXPIRED')"`
	Account            string `gorm:"type:varchar(64)"`
	AccountID          uint
	Currency           string `gorm:"type:varchar(225)"`
	AccountPath        string `gorm:"type:varchar(64)"`
	BankCode           string
	BankAccount        string
	BankName           string
	AccountHoldName    string
	Description        string
	Provider           string
	RequestId          string
	FailReason         string
	Fee                uint64
	HexSignTransaction string
}
